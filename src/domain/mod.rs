//! Domain-specific modules.

pub mod imap;
pub mod mbox;
pub mod msg;
pub mod smtp;
