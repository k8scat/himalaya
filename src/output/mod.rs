//! Module related to output formatting and printing.

pub mod arg;
pub mod service;
pub mod utils;
